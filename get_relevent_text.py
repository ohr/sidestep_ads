#!/bin/python3
# get_relevent_text.py
#
# Omkar H. Ramachandran
# ramach21@egr.msu.edu
#
# Simple script to cut out all the scripts from a news website and just get
# the text of the article
#

import sys

input_file = sys.argv[1]
output_file = sys.argv[2]
relevant_lines = []

with open(input_file) as fp:
    lines = fp.readlines()
    for line in lines:
        flag = 1
        while(flag != -1):
            # Paragraph
            start = ["<p>","<ul>","<li>","<p ","<ul ","<li "]
            end = ["</p>","</ul>","</li>","</p>","</ul>","</li>"]
            i = 0
            for i in range(len(start)):
                index1 = line.find(start[i])
                if(index1 == -1):
                    continue
                else:
                    index2 = line.find(end[i])
                    break
            if((index1 == -1) or (index2 == -1)):
                flag = -1
            else:
                relevant_lines.append(line[index1:index2+len(end[i])])
                line = line[index2+4:]

with open(output_file,"w") as fout:
    fout.write("<html>\n")
    fout.write("<head><title>News channels that block ads suck</title></head>\n")
    fout.write("<body style=\"background-color:black;\" link=\"white\">\n")
    fout.write("<font color=\"grey\">")
    for i in range(len(relevant_lines)):
        fout.write(relevant_lines[i]+"\n")
    fout.write("</font>")
    fout.write("</body>\n")
    fout.write("</html>")
