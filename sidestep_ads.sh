#!/bin/bash

# Input file
URL=$1

mkdir /tmp/sidestep_ads

wget -O /tmp/sidestep_ads/infile.html $URL

INFILE=/tmp/sidestep_ads/infile.html
OUTPUTFILE=/tmp/sidestep_ads/outfile.html
get_relevent_text.py $INFILE $OUTPUTFILE

firefox $OUTPUTFILE &

